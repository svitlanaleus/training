import org.junit.Test;

import static org.junit.Assert.*;

public class AvgTest {

    @Test
    public void testAvgForInteger() throws Exception {
        assertEquals(5, new Avg().Avg(7, 3));
    }

    @Test
    public void testAvgForZero() throws Exception {
        assertEquals(0, new Avg().Avg(0, 0));
    }
    @Test
    public void testAvgForNegative() throws Exception {
        assertEquals(-5, new Avg().Avg(-7, -3));
    }
}